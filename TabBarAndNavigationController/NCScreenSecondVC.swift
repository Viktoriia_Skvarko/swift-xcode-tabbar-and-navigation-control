//  NCScreenSecondVC.swift
//  TabBarAndNavigationController
//  Created by Viktoriia Skvarko


import UIKit

class NCScreenSecondVC: UIViewController {
    
    @IBAction func buttonHouse(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
