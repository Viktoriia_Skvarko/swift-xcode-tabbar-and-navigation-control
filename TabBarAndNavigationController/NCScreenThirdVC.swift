//  NCScreenThirdVC.swift
//  TabBarAndNavigationController
//  Created by Viktoriia Skvarko


import UIKit

class NCScreenThirdVC: UIViewController {
    
    @IBAction func home(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func buttonViewFirstScreen(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
