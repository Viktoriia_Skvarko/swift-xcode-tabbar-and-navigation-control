//  PageFirst.swift
//  TabBarAndNavigationController
//  Created by Viktoriia Skvarko


import UIKit

class Page: UIViewController {
    var titleLabel: UILabel?
    
    var page: Pages
    
    init(with page: Pages) {
        self.page = page
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        titleLabel?.textAlignment = NSTextAlignment.center
        titleLabel?.text = page.name
        titleLabel?.backgroundColor = page.colorVC
        self.view.addSubview(titleLabel!)
    }
}
