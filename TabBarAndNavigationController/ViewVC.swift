//  ViewVC.swift
//  TabBarAndNavigationController
//  Created by Viktoriia Skvarko


import UIKit

class ViewVC: UIViewController {
    
    @IBAction func alertView(_ sender: UIButton) {
        alertViewAction()
    }
    
    func alertViewAction() {
        let alertOneDo = UIAlertController(title: "Соoбщение", message: "Ваше настроение хорошее?", preferredStyle: .alert)
        alertOneDo.addAction(UIAlertAction(title: "Конечно", style: .cancel))
        present(alertOneDo, animated: true)
    }
    
}
